
const response = {"apiRoutes":{"/pet":{"GET":{"description":"Get pet details by id","request":{"query":{"id":{"required":true,"type":"int"}},"body":{}},"response":{"code":200,"data":{"pet":{"id":1,"type":"Dog","name":"Stella"}}}},"POST":{"description":"Create a new pet and return id","request":{"query":{},"body":{"pet":{"type":"Cat","name":"Mirchi"}}},"response":{"code":200,"data":{"pet":{"id":2}}}}},"/customers":{"GET":{"description":"List all customers","request":{"query":{},"body":{}},"response":{"code":200,"data":{"customers":[{"id":1,"name":"John Doe"},{"id":2,"name":"Jane Doe"}]}}}}}};

let rootNames = [];
let rootCont = [];
let apiroutes = response.apiRoutes;
let subRoots = [];
let subrootNames = [];
let names = [];
let methods = [];
createLeftPanel();


function createLeftPanel(){
    let routes = document.getElementById("routes");
    
    
    let navStr =  "";
    let count = 0;
    let str =  "";
    Object.keys(apiroutes).forEach(function(key) {
        rootNames[count] = key;
        rootCont[count] =  apiroutes[key];
        count ++;
    });
   
    count = 0;
    for(let i=0;i<rootCont.length;i++){
        let sub = rootCont[i];
        
        Object.keys(sub).forEach(function(key) {
        subRoots[count] = sub[key];
        subrootNames[count] = rootNames[i];
        str =  "<li onClick='createRight("+count+")'>"+key+rootNames[i]+"</li><br><br>";
        navStr = navStr + str;
       methods[count] = key;
       
        count ++;
      });
    }

    routes.innerHTML = navStr;
}

function createRight(value){
let frmDiv =  document.getElementById("frmDiv");

 let mainStr = ""
 let paraStr = "";
 let body = "";
 let required = "";
 let desc = subRoots[value].description; 
    if(methods[value]=="GET" &&  !isEmpty(subRoots[value].request.query)){
      paraStr  = subRoots[value].request.query.id.type+" "+subRoots[value].request.query.id.required
    }
   if(methods[value] == "POST"){
    
    required = "*required";
   
   let val = subRoots[value].request; 
   console.log(val,desc)
   body = JSON.stringify(val.body,undefined,2);
   
       
    }
    
               

 
 let str = "<form class='sub-article' > <div class='background'> <button  class='green'>"+methods[value]+"</button> "+subrootNames[value]+" &nbsp;&nbsp;&nbsp; "+desc+" </div> <div class='text'>Parameters : "+paraStr+"</div> <div class='background'>Name &nbsp; &nbsp; Description <hr class='blackline'> <section class='block'>body <span style='color:red'> "+required+"</span> &nbsp;&nbsp;&nbsp; "+desc+" <br>object<br>(body) &nbsp;&nbsp;&nbsp; Example Value <article class='request'> <pre>"+body+"</pre>  </article> </section> </div> <div class='text'>Responses</div> <div class='background2'>Code &nbsp; &nbsp; Description <hr class='blackline'> <section class='block'>"+subRoots[value].response.code+" &nbsp; &nbsp; Example Value <article class='response'> <pre>"+JSON.stringify(subRoots[value].response.data,undefined, 2)+"</pre></article> </section> </form>"
 frmDiv.innerHTML = str;
}

function isEmpty(obj) {
    for(let prop in obj) {
      if(obj.hasOwnProperty(prop)) {
        return false;
      }
    }
  
    return JSON.stringify(obj) === JSON.stringify({});
  }